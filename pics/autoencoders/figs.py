import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.decomposition import PCA


if __name__ == "__main__":

    markerSize = 40
    fs1 = 22
    fs2 = 18

    N = 200


    def plot_stuff(x1,y1,x2,y2):
        x = np.concatenate([x1,x2])
        y = np.concatenate([y1,y2])
        V = np.array([x,y]).T

        x1-=np.mean(x); x2-=np.mean(x)
        y1-=np.mean(y); y2-=np.mean(y)

        fig1,axs1 = plt.subplots(1,2)
        fig1.subplots_adjust(bottom=0.15)

        axs1[0].scatter(x1,y1,c='k',s=markerSize)
        axs1[0].scatter(x2,y2,c='r',s=markerSize)
        

        axs1[0].set_xlabel('Feature 1',fontsize=fs1)
        axs1[0].set_ylabel('Feature 2',fontsize=fs1)
        axs1[0].set_xlim([-6,6])
        axs1[0].set_ylim([-6,6])


        pca = PCA().fit(V)
        pc1 = pca.components_[0,:]*5
        axs1[0].plot([0,pc1[0]],
                    [0,pc1[1]],'-',c='b',lw=3)
        leg=axs1[0].legend(['First PC','Class 1','Class 2'],prop={'size':14},loc='upper left')
        axs1[0].plot([0,-pc1[0]],
                    [0,-pc1[1]],'-',c='b',lw=3)


        projections = V.dot(pc1/5)
        projectionsC1 = projections[:N]
        projectionsC2 = projections[N:]
        bins = np.linspace(np.min(projections),np.max(projections),31)
        axs1[1].hist(projectionsC1,bins,color='k',alpha=0.5)
        axs1[1].hist(projectionsC2,bins,color='r',alpha=0.5)
        axs1[1].set_xlabel('PC1 projection',fontsize=fs1)
        axs1[1].set_ylabel('Frequency',fontsize=fs1)
        

        ticks = []
        for ax in axs1:
            ticks += ax.get_xticklabels() + ax.get_yticklabels()
        
        _=map(lambda x:x.set_fontsize(fs2),ticks)

    x1 = np.random.randn(N)
    y1 = (x1)*np.sqrt(.25) + np.random.randn(N)*np.sqrt(.75)
    
    x2 = np.random.randn(N)
    y2 = (x2)*np.sqrt(.25) + np.random.randn(N)*np.sqrt(.75)

    x1-=1; y1-=1
    x2+=1; y2+=1

    plot_stuff(x1,y1,x2,y2)


    x1 = np.random.randn(N)
    y1 = (x1)*np.sqrt(.75) + np.random.randn(N)*np.sqrt(.25)
    
    x2 = np.random.randn(N)
    y2 = (x2)*np.sqrt(.75) + np.random.randn(N)*np.sqrt(.25)

    x1-=.5; y1+=.5
    x2+=.5; y2-=.5

    plot_stuff(x1,y1,x2,y2)
    

    plt.show(block=False)
