---
layout: post
title: "The graphical explanation to why most points lie on a shell in high-dimensional Gaussians"
date:   2018-02-14 17:00:00 +0000
categories: datascience mathematics statistics
mathjax: true    
---

One of the most fascinating cases of how low-dimensional reasoning fails to generalise to higher dimensions is how distances behave in high dimensions. This is especially the case with high-dimensional Gaussians. With high-dimensional Gaussians, as it turns out, the vast majority of points lie on a shell around the centroid of the Gaussian. This seems completely counterintuitive since in one, two, and three dimensions, Gaussians are bell curves, blobs, and spheres, respectively. 
    
So why does this happen? I have a slightly simplistic answer to this. Let us start with a
two-dimensional Gaussian since this is easy to visualise. Suppose we have a
number of points from a two-dimensional Gaussian, as in the left plot below.

![png](/pics/multidim_gaussian_graphical/output_2_0.png)


The distance here is shown in red and we can compute it using the Pythagorean theorem. The above
point is (0.36,-1.03), and so the length of the vector is just
\\(d = \sqrt{0.36^2 + (-1.03)^2 } = 1.09\\).
The right plot shows the distribution of distances for all of the points in the left plot. As
we move to higher dimensions, two things will happen. First, the average distance away from the
Gaussian will increase. This is because distance is defined as 

$$
d = \sqrt{\sum_j^N x_j^2},
$$

and so the average distance increases with the square root of the number of dimensions. Second,
the standard deviation around the mean will also change. Importantly, given a fixed average distance,
the standard deviation will be *smaller* for higher dimensions. We can visualise this by plotting the
distribution of distances for a fixed distance from the origin for Gaussians of different
dimensionality.


```python
%matplotlib inline
import numpy as np
import matplotlib.pyplot as plt

cols = [(0.9,0.6,0.1),(0.1,0.5,0.3),(0.9,0.1,0.8)]
N = 100000
fig,ax = plt.subplots(1,figsize=(6.5,5.5))
ks = [1,3,10]
for i,k in enumerate(ks):
    y = np.sum(np.sqrt(np.random.exponential(1,(k,N))*2),axis=0)
    y = y/np.mean(y)
    bins = np.linspace(0,5,51)
    a,b = np.histogram(y,bins,normed=True)
    b = b[1:]+(b[2]-b[1])/2
    ax.plot(b,a,'-',lw=3,label='K=%i'%k,c=cols[i])
    
ax.set_xlabel('Distance',fontsize=18)
ax.set_ylabel('Probability density',fontsize=18)
ax.set_yticks(np.arange(0,3.0,0.5))
ax.set_ylim([0,2.6])
ax.set_xlim([0,3.5])
ticks=ax.get_xticklabels() + ax.get_yticklabels()
_ = list(map(lambda x:x.set_fontsize(16),ticks))
_=ax.legend(fontsize=16)
```


![png](/pics/multidim_gaussian_graphical/output_4_0.png)

In other words, as you move from a Gaussian with low dimensionality (e.g. orange line) to one with
high dimensionality (e.g. magenta line), you expect to find yourself closer and closer to the
average distance (which is around 1 above). In fact, the reason for this is just the [central limit
theorem](https://en.wikipedia.org/wiki/Central_limit_theorem). Nhe central limit theorem
states that if you draw \\(N\\) random samples from any arbitrary distribution, then the resulting
distribution of the mean of those draws will be normally distributed. Further, the expected value of
that distribution will simply be the mean of the underlying distribution, and the standard will be
 \\(\frac{\mathrm{SD}[X]}{\sqrt{N}}\\), where \\(\mathrm{SD}[X]\\) refers to the SD of the underlying
distribution, and \\(N\\) is the number of dimensions. In other words, the more samples you draw,
the smaller the standard deviation will become. Or in this case, the higher the dimensionality of
your Gaussian, the closer to the average distance from the mean the point will be.
