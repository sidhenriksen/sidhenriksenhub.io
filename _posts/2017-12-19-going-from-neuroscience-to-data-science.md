---
layout: post
title:  "Making the move from neuroscience to data science"
date:   2017-12-19 21:00:00 +0000
categories: datascience neuroscience
---
Having just submitted the corrections to my PhD thesis, I figured I would write a celebratory post
about my experience
moving from academic neuroscience to data science in the private sector.

People's motivations for
moving out of academia vary a whole lot and mine were basically the usual suspects: poor job
security in academia,
wanting to control where I lived (in academia you generally go where you can get a gig), wanting
a more social working pattern, and wishing
to do something of practical value. In terms of practical value, you can, of course,  develop a
translational research career in
academia, and this is something I am still considering. However, data science provides a platform
for qucikly generating real world impact using science, technology, and mathematics, which has
enormous appeal to me. A short project (e.g. 2-3 months) can almost immediately begin generating
real value, which is something that is pretty alien in the academic world.

For people familiar with data science, the following may seem obvious, but I am writing this with
the assumption that you are an MSc/PhD student or a recent graduate who is evaluating their career
options. Many of these skills can (and some of them arguably should) be developed during the
course of your academic career. However, that being said, you should always keep in mind the notion
of a "minimum viable
data scientist" - i.e. what do you need to know in order to be hirable as a data scientist? That
will depend on your background, but I think if you are able to at least partially tick the following
categories, you will be a very competitive candidate for most jobs, especially in the UK data science
market.

## Programming
A strong programming background is key to a data science career. A huge amount of
professional data work involves developing efficient processing pipelines which do not
become unmaintainable behemoths. If you are in neuroscience, you may have learned Matlab (as I did).
If it is not too inconvenient, I would encourage you to switch to a more modern programming language
like Python or R. These are by far the most common languages in data science. If you can't switch
to either of these in your main work, I highly recommend learning these in your spare time. And as
always, the best way to learn a programming language is to build something. If you are stuck with
some proprietary language like Matlab, I would urge you to learn as much about it as you can.
For example, during my PhD, I developed a toolbox for simulating neurons in primary visual cortex
of the primate brain. I used this toolbox quite extensively (though no one else did), but mostly
it taught me a huge amount of object oriented programming and UX design (even if the users were,
and remain, largely imaginary).

## Software carpentry
[Software carpentry](https://software-carpentry.org/lessons/) is a massively overlooked skillset,
and most scientists have absolutely no clue about any of this. These skills include things like
knowing your way around a Unix shell, working effectively with version control (e.g. Git,
Subversion, Mercurial), automated testing of code (e.g. unit testing), writing modular,
comprehensible, and commented code, and so on. The difference
between life as an MSc/PhD student and that of a professional data scientist is night and day
when it comes to code. As a PhD student, you will write code that very few people (if any) will
actually read. As a professional data scientist, your code will be read and re-read many times,
and your code will also be built on and extended. Writing clear, concise, well-documented code that
is maintained by version control is pretty essential for this stuff.

## Machine learning and statistics
Knowing your way around machine learning is obviously pretty key to being an effective data
scientist. I would highly recommend Andrew Ng's course on Coursera. The course does not shy away
from equations, but it is not very mathematically sophisticated. If you struggle to follow the
details, I would consider taking a course in calculus and/or linear algebra (even as revision if
it has been a while). An often overlooked, but
closely related skill is general fluency in statistics and probability. Building a strong foundation
in probabilistic thinking is incredibly valuable when it comes to testing hypotheses you might have
about your data (or the world more genenrally). A question I keep asking myself in my own work is
"What does chance look like?" and
very often, it is not obvious what the null hypothesis actually is. Having a firm foundation in
statistics allows you to address these sorts of questions.

## Visualisations
I consider this to be one of the key ways in which data scientists not only communicate insights,
but are also able to productionise their work. A great way to do this is to make interactive web apps
or dashboards using JavaScript, Python, or R. Plotly have phenomenal packages for doing this in
both Python ([Dash](https://plot.ly/dash/)) and R ([Shiny](https://plot.ly/r/shiny-tutorial/)).
I have a couple of examples of Dash apps listed in
[my portfolio](https://sidhenriksen.github.io/portfolio) and I highly recommend developing these
skills. Frameworks such as Flask in Python also provide a great way to make APIs for deploying
machine learning models.

## Soft skills
Technical skills are incredibly important for data scientists, but having the ability to interpret
and communicate findings or ideas is also essential. Your line manager may be a domain expert rather
than a data scientist, or someone who has a "business intelligence" background. However, they may
perhaps not possesss the
sort of statistical, programming, or machine learning competence that you bring to the table.
Being able to pitch the importance of building a particular statistical model is incredibly
valuable, and that often means acting as the interface between the organisation's business problems
and the data science solutions.


This list is wholly a reflection of my experience going from a neuroscientist to a data scientist.
As a PhD student, I focused
a lot on developing machine learning and statistics skills, and I have only later emphasised
the software engineering components. Everyone's journey will be different, of course, and you'll
never learn everything. I think the a key characteristic I have noticed in successful data
scientists is that they are always keen to learn new things and improve their existing way of
doing things. If you can embrace that philosophy and learn some technical bits on the way, you'll
be well on your way.