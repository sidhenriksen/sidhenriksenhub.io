---
layout: post
title:  "The sociology of PhD studentships"
date:   2017-10-16 20:20:38 +0100
categories: academia science neuroscience
---

The vast majority of scientific research in the world is done by PhD students. PhD students form 
the backbone of science, and if they were to strike, science would immediately come grinding to a 
halt. If PhD students are just scientists, why are they called "students"?

Well, this has to do 
with the awarding of the PhD itself - in order to study for a doctorate, you of course have to be 
a "student". And the UK takes this semantic accident seriously: PhD students are paid a tax-free 
stipend, are not considered employees of the university, do not make national insurance 
contributions, are not entitled to holiday or sick leave, are not entitled to parental leave, and 
do not pay council tax. In the UK, there is no distinction between a PhD student and an 
undergraduate student. In practice, however, there is a massive difference: PhD students do not 
attend lectures or have exams, whereas undergraduates do. In fact, they don't have courses or a 
curriculum at all. A PhD is a research degree, and so doing original research is what you spend 
your entire time doing. Many PhD students even co-supervise undergraduate and master's students. 
The closest thing to the role of a PhD student is the postdoctoral researcher - the position that 
immediately follows a PhD student in the academic hierarchy. Many countries have recognised this 
fact, and have acknowledged that the term "PhD student" is merely a title. In Denmark, 
<a href="http://talent.au.dk/phd/scienceandtechnology/financing/">"PhD students are employed on 
the basis of academic trade union agreements"</a>, and in Germany 
<a href="https://www.mpg.de/career/how-do-i-get-supported">a similar scheme exists</a>. In other 
words, PhD students are employees who receive all the benefits and responsibilities of employment.

Most PhD students in the UK are on the RCUK minimum. This minimum is £14,553 a year; the pre-tax
equivalent for someone paying NI and income tax would be around £16,600 (not factoring in council 
tax; you can probably add £500-£1,000 if you want to add that in, depending on where you live). 
For a PhD student who works 40 hours a week (probably an underestimate of the average), and who 
works 48 weeks of the year, this works out to £8.64 an hour. Now, this is just above the living 
wage as defined by the <a href="https://www.livingwage.org.uk/">Living Wage Foundation</a>, but 
is hardly the sort of salary you would expect that some of the brightest graduates in the country 
would be offered. A first year postdoc can make almost twice as much as a final year PhD student 
(in terms of effective pre-tax income), despite there being nothing meaningfully separating them. 
The lower band of postdoc salaries generally range between £27,000 and £32,000.

The issue is not just PhD pay, which is abysmally low, but also the rights of PhD students. In the 
UK, if you work 5 days a week, you are entitled to 28 days paid leave a year. As a PhD student, you 
have no such rights, and holiday leave is basically granted at the discretion of your PhD supervisor.
To be sure, most PhD supervisors are not malevolent and so will grant fairly generous annual leave 
should you request it, but this is not always the case, and it is well-known in the tech sector that
if you offer unlimited leave to employees, they often end up taking fewer days off. It would not be 
at all surprising if a similar phenomenon exists for early-career scientists. Parental leave is 
entirely at the discretion of the funding body, and while organisations such as Research Councils 
UK (RCUK)  provide 6 months full-stipend maternity cover, it is worth keeping in mind that this is 
largely a gesture. The RCUK stipend is currently £14,553 per annum. The average cost of child care 
in the UK (assuming 48 weeks of the year) is £10,673 per annum. This is not a post about the 
extortionate cost of child care in the UK, but suffice it to say that you do not need to be a 
mathemagician to see that those numbers are going to make things very tight once the student 
returns from maternity leave, even with a partner involved. RCUK basically count on PhD students 
not taking maternity leave since they are in no financial position to be parents. Statutory sick
pay does not exist through DWP for PhD students since they do not technically work, and SSP is
awarded at the discretion of the funding agency.  The UK could be 
a world leader in offering their scientists competitive packages, but instead, they are among the 
worst offenders in the West.

So why do we persist with the PhD *student* model? That is a complicated question. Part of 
the answer is purely economical: because of the culture of PhD studentships, you can get away with
paying highly qualified scientists around minimum wage. The people who are worried about balancing
research budgets are not very keen on paying scientists more money (even if it means a fair wage).
Another part of the answer lies in the fact that considering pre-doctoral researchers 
as students allows universities to charge tuition fees, which are paid by the student's funding body 
(or in extreme cases, by the student themselves). This money then goes to faculty or department where
the student is based. So what's the problem with charging PhD students tuition? The problem is that
PhD students do not attend lectures, do not have lab practicals and do not need their essays marked.
PhD students provide the bulk of actual lab work in most universities. Imagine if large corporations
began charging graduates several thousand pounds a year for attending their graduate training
programmes. This would probably meet legal resistance, not to mention public outrage. The analogy
is slightly misleading, since only a minority of researchers pay their own way through their PhD,
but it does happen. It is also a massive barrier to entry for talented non-EU scientists who
have to work out how to pay uptowards £15,000 a year. If the UK is to retain its status as a
world leading scientific nation, a major step towards that would be to get rid of tuition fees for
PhD studentships and open the competition up to attract international talent.

The poorly funded PhD studentships are a symptom of a greater structural problem in academic 
science: many countries [are simply producing too many PhDs](http://www.universityworldnews.com/article.php?story=20130403121244660). Intuitively, this should be a 
positive thing since this means a more educated workforce. However, the type of education
matters enormously. If we produced 50,000 people with PhDs in physics and applied mathematics,
we would likely see a very different outcome than if we produced 50,000 people with PhDs in
literature. Presently, the growth in PhDs far exceeds the growth in faculty positions, which
means that a large number of PhDs will necessarily have to find jobs in either the private
or public sector. So the question becomes, can we improve the allocation of research money,
which both improves research quality and researcher welfare? I believe the answer to this is yet
and will discuss this in a subsequent post.
