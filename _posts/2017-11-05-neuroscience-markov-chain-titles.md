---
layout: post
title:  "Generating neuroscience paper titles with Markov chains"
date:   2017-11-05 14:00:00 +0100
categories: datascience neuroscience
---

Markov chains are a lot of fun. You will find a huge number of Twitter bots and similar
which make use of them for comedic effect. A Markov chain is a really simple probabilistic
system which navigates from one state to another with a certain probability.

In the figure below, there are three states. If I find myself in State 1, then I have a 25%
chance of changing to State 3 (black arrow) and 75% chance of changing to State 2. The Markov
chain has no memory of this process, so if it navigates to State 2, it could very well just navigate
straight back to State 1. 

![Markov chain](/pics/markov-chain-title/markov-chain.jpg)

This is useful since it allows us to capture some rudimentary structure about the system.
For example, if I have just said "the car", then a Markov chain will capture the rudimentary idea
that "broke" is more likely to follow "car" than "potato". So Markov chains allow you to generate
the semblance of language, despite being pretty unsophisticated abouto it.

I was looking for a weekend project and decided to build a Markov chain to generate synthetic
neuroscience paper titles. Neuroscience, like all fields, can be incredibly esoteric and filled
with jargon. The fact that they are so inaccessible makes them a prime candidate for Markov chains
since most people don't really know whether the title
"Connexin43–src interaction deficits induces offspring cerebral insult" makes sense.

So I went to sciencedirect.com and asked for a large number of titles from the journals Neuron
and Neuroscience. You can actually download these things in large numbers free of charge since
academics use it for building their reference libraries (e.g. in Latex or Word). I parsed the
titles (and the abstracts - maybe there is a future project using the abstracts), and built
a Markov chain in Python. The code for this is on [Github](https://github.com/sidhenriksen/markov-chain-neuroscience-titles). I have just over 4000 titles published in the last 7 or so years.

Here are some my favourites:

> Phosphoinositide lipids in hipposideros pratti

> Thrombin-par1-pkcθ/δ axis activation during sharp wave-ripples

> mouse midbrain 5-ht1a function during choice reaction

> macaque dorsolateral pre frontal gyrus output impairs the battle over conflict during repetitive
> magnetic resonance spectroscopy (fNIRS)

This last one is great, because it actually sort of makes sense: the dorsolateral prefrontal gyrus
is a very important area in the frontal cortex which is famously implicated in working memory and
other "higher" functions. However, this sentence also highlights the issue with
Markov chains: magnetic resonance spectroscopy is absolutely a thing, but fNIRS is short for
functional near-infrared spectroscopy. The Markov chain knows the relationship between spectroscopy
and fNIRS, but has no memory. Of course, I'm not quite sure what "impairs the battle over conflict"
is meant to mean, but hey. And to illustrate some of the more blatant failures:

> Changes and neuropathy: time flexibly determines cell-specific knockout enhances posterior
> pedunculopontine tegmental nucleus remain viable

Here, the MC is doing really well at first: "Changes and neuropathy: time flexibly determines
cell-specific knockout". This is a completely okay sentence; it doesn't make any sense, but it's
acceptable English. But then because the Markov chain doesn't have any memory, it doesn't know
what it has just said. So it proceeds to say "knockout enhances...", which is the beginning of
a completely different sentence.

Anyway, Markov chains are a lot of fun, but they also actually have practical uses. One really
important application is for what is known as Markov chain Monte Carlo methods (MCMC).
MCMC is a way of sampling from distributions for which it is very difficult to write down (and
evaluate) an expression. A large number of algorithms exist which can arrive at a particular
distribution through a Markov chain. The "Monte Carlo" component of MCMC comes from the fact
that these methods do Monte Carlo simulations (i.e. sampling)
on distributions which are generated through a Markov chain.