---
layout: page
title: Curriculum Vitae
permalink: /cv/
---
# Education
* **PhD Neuroscience**, Newcastle University and the National Institutes of Health (2017)    
	Thesis: The neurophysiology of stereoscopic vision
* **MRes Computational Biology**, Newcastle University (2014)    
	Completed with distinction
* **BSc (Hons) Psychology**, University of Aberdeen (2013)    
	Completed with first class honours

Additional courses completed in mathematics and statistics, finance, computing science, neuroscience, and computational biology.

# Experience
* **Independent data science consultant** (2018 - present)    
    Currently on contract to Royal Bank of Scotland, developing machine learning-based technology
    for detecting money laundering
* **Data scientist**, Tesco Bank (2017 - 2018)    
    Developed machine learning-based system for detecting fraudulent credit card transactions. Also developed apps for viusalising
    current and potential profit positions to stakeholders.
* **Data scientist** (3 month internship), Machine Delta (2017)    
    Worked on automated quality assurance of anti-money laundering processes.
* **Scientific programmer**, Newcastle University (2016 - 2017)    
    Provided programming and technical expertise to research group.

# Core competencies
**Machine learning and analytics.** Extensive experience applying cutting edge machine learning
    techniques to an eclectic set of problem areas with particular expertise within financial
    services and banking. Well-developed analytical skillset and data
    visualisation abilities (see [portfolio](/portfolio.html) for examples).
    
**Programming.** Expert knowledge of 
    Python, particularly the Python data science
    stack (sklearn, numpy, pandas, etc.). Well-versed in Matlab, and have written a
    [toolbox](https://github.com/sidhenriksen/bemtoolbox) based on my PhD work.
    Good command of software engineering principles such as
    unit testing and version control. Very literate in R and Shiny, and comfortable with
    both SQL and SAS.

**Writing and communication.** First author of numerous scientific publications, and have presented
    scientific work at conferences and events to both academics and laypersons. Keen understanding
    of concise and clear technical writing. Passionate about understanding topics well and
    communicating them clearly.
        
# Awards
* Doctoral Thesis Prize for Faculty of Medical Sciences, Newcastle University (2018)
* Newcastle Biomedicine MRes International Student for outstanding effort (2014)
* Best research project at the Allen Institute Dynamic Brain workshop (2014)
* University of Aberdeen Alan B Milne prize for best undergraduate thesis in psychology (2013)


# Selected academic publications
**Henriksen, S.**, Read, J. C., & Cumming, B. G. (2016).
Neurons in striate cortex signal disparity in half-matched random-dot stereograms.
*Journal of Neuroscience, 36(34)*, 8967-8976.
<a href="http://www.jneurosci.org/content/36/34/8967.short">
<i class="ai ai-open-access">Open access.

**Henriksen, S.**, Cumming, B. G., & Read, J. C. (2016).
A single mechanism can account for human perception of depth in mixed correlation
random dot stereograms. *PLoS Computational Biology, 12(5)*, e1004906.
<a href="http://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1004906">
<i class="ai ai-open-access">Open access.

**Henriksen, S.**, Pang, R., & Wronkiewicz, M. (2016).
A simple generative model of the mouse mesoscale connectome.
*ELife, 5*, e12366.
<a href="https://elifesciences.org/articles/12366v1">
<i class="ai ai-open-access">Open access.

For a complete list, see my <a href="https://scholar.google.com/citations?user=o0vO4RYAAAAJ">
Google scholar profile</a>
