---
layout: page
exclude: true
mathjax: true
---
<script src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-AMS-MML_HTMLorMML" type="text/javascript"></script>

## Cost functions and derivation of backpropagation
The following is incredibly long-winded, but if you've found your way to this page, it's worth noting that this is my sketchpad more than anything. The reason this is very long-winded is that I really wanted to make sure I understood neural nets well enough to derive the cost function derivatives and do backpropagation from scratch. Pretty much none of this stuff is done with the help of a textbook, which is great for my own educational purposes, but makes pretty crap reading. It is also almost certainly not the most concise derivation.

On the other hand, if you're looking to understand mathematically what backpropagation is doing, this is probably not a bad place to start. It only requires calculus and a little bit of linear algebra, and probably a lot of patience.

## Choice of activation function
Traditionally, standard sigmoids were early choices for the activation function. However, in practice, the hyperbolic tangent appears to work better

$$
\tanh(z) = \frac{e^z - e^{-z}}{e^z + e^{-z}}.
$$

However, both the tanh function and the sigmoid have gradients very close to zero for large or small values of \\(z\\) making training difficult. In the case of deep architectures, it also exacerbates the problem of vanishing gradients.

The most popular choice for the activation function is now the rectified linear unit (RELU). This is defined as

$$
\mathrm{RELU}(z) = \max(0,z).
$$

However, a potential problem with the RELU is that for \\(z < 0\\), \\( \frac{\partial \mathcal{L}}{\partial z} = 0 \forall z < 0\\) (i.e. the slope is zero for values less than 0, which means that an infinitesmal change in the input can never have any impact on the cost). To deal with this problem, the leaky RELU has been proposed,

$$
\mathrm{RELU}_\mathrm{leaky}(z; \alpha) = \max(\alpha z, z),
$$

where \\(\alpha\\) is a free parameter (a good initial choice for this is 0.01).

Both these RELU variants have the problem that their derivatives are ill-defined for \\(z=0\\). However, in practice, it is incredibly improbable that \\(z\\) ends up being *exactly* zero, so we do not worry about it. Let's plot these activation functions.


```python
%matplotlib inline
import matplotlib.pyplot as plt
import numpy as np

def set_ticklabelsize(axs,fs):
    if not np.iterable(axs):
        axs = [axs]
        
    ticks = []    
    for ax in axs:    
        ticks += ax.get_xticklabels() + ax.get_yticklabels()
    
    _ = list(map(lambda x:x.set_fontsize(fs), ticks))

alpha = 0.1
z = np.linspace(-1,1,501)
fs = [lambda z:1/(1+np.exp(-z)),
     lambda z: (np.exp(z)-np.exp(-z)) / (np.exp(z)+np.exp(-z)),
     lambda z: z * (z > 0),
     lambda z: z * (z > z*alpha) + z*alpha * (z < z*alpha)
     ]

displayCoeffs = [5,3,1,1]
fig,axs = plt.subplots(2,2,figsize=(12.5,10.5))
axs = axs.ravel()
ctr = -1
ticks = []
for ax,f in zip(axs,fs):
    
    ctr+=1
    c = displayCoeffs[ctr]
    y = f(z*c)
    ax.plot(z,y,'-',lw=3,c='k')
    
    if ctr in [0,2]:
        ax.set_ylabel('Activation',fontsize=16)
    if ctr in [2,3]:
        ax.set_xlabel('z',fontsize=16)
    
    if ctr == 1:
        ax.set_yticks(np.arange(-1,1.5,0.5))
    else:
        ax.set_yticks(np.arange(0,1.25,0.25))
        
    ax.set_xticks(np.arange(-1,1.5,0.5))
    
    
set_ticklabelsize(axs,16)
```


![png](output_2_0.png)


## Derivatives of activation functions

### Sigmoid
For the sigmoid, as I showed in the last week's notes, the derivative of the sigmoid is simply
    
$$
\frac{dg}{dz} = g'(z) = g(z) (1-g(z)) = a (1-a)
$$

### Hyperbolic tangent
I won't go through the derivation of this, but it can be shown that the derivative of the hyperbolic tangent is

$$
g' (z) = 1 - [\tanh(z)]^2.
$$

### Rectified linear unit
For the RELU, this is really simple

$$
g'(z) = 
\begin{cases}
0 & \mbox{if } z < 0 \\
1 & \mbox{if } z > 0 \\
\mbox{undefined if } z = 0.
\end{cases}
$$

Similarly, for the leaky RELU,

$$
g'(z;\alpha) = 
\begin{cases}
\alpha & \mbox{if } z < 0 \\
1 & \mbox{if } z > 0 \\
\mbox{undefined if } z = 0.
\end{cases}
$$

In practice, we will just ignore the bit where the derivative is undefined and set the derivative to 1 in this case (if it ever happens).


```python
def sigmoid_deriv(z):
    return z*(1-z)

def tanh_deriv(z):
    1 - np.tanh(z)**2

def relu_deriv(z):
    return np.double(z>=0)

def leaky_relu_deriv(z,alpha=0.01):
    return np.double(z>=0) + np.double(z<=0)*alpha
```

## Forward computation of responses
This is a straightforward extension of logistic regression. With an input matrix \\(X\\), we have

$$
Z^{[1]} = W^{[1]}X + b^{[1]},
$$

where \\(W\\) and \\(b\\) denote the weight matrix and bias, respectively. The square bracketed superscripts indicate the layer. We pass this through the activation function to obtain \\(A\\)

$$
A^{[1]} = g^{[1]}(Z^{[1]}).
$$

The activation function \\(g\\) is also subscripted since we can have different activation functions for different layers. This basic architecture is now simply repeated. For subsequent layers, we simply do

$$
A^{[i]} = g^{[i]}\left( W^{[i]}A^{[i-1]} + b^{[i]} \right)
$$

# Backward propagation of errors
We will assume binary classification, and define the derivative with respect to the error in the final layer

$$
\frac{d \mathcal{C}}{d \mathbf{a}^{[L]}},
$$

where \\(L\\) is the number of layers. This is not a partial derivative since the elements of the vector \\(\mathbf{a}\\) correspond to different training examples, not parameters of the network. Using vector notation up front makes the Python implementation easier later on. 

We will use the chain rule to backpropagate the error, and we want to know how the cost function changes as we adjust the weights of the network

$$
\frac{\partial \mathcal{C}}{\partial \mathbf{w}^{[L]}} = 
\frac{d \mathcal{C}}{d \mathbf{a}^{[L]}} \frac{\partial \mathbf{a}^{[L]}}{\partial \mathbf{w}^{[L]}}.
$$

\\(\mathbf{w}^{[L]}\\) refers to the weights connecting layer \\(L-1\\) to layer \\(L\\). This is a vector and not a matrix since we only have a single node in the final layer. We can similarly adjust the weights connecting layer \\(L-2\\) to layer \\(L-1\\), which we denote as \\(W^{[L-1]}\\) (note that this is a matrix since layers \\(L-1\\) and \\(L-2\\) will both generally have more than one unit each). 

$$
\frac{\partial \mathcal{C}}{\partial W^{[L-1]}} = 
\frac{\partial \mathcal{C}}{\partial A^{[L-1]}} \frac{\partial A^{[L-1]}}{\partial W^{[L-1]}}.
$$

Now we need to work out how the cost changes with activation in layer \\(L-1\\),

$$
\frac{\partial \mathcal{C}}{\partial A^{[L-1]}} =
\frac{d \mathcal{C}}{d \mathbf{a}^{[L]}} \frac{\partial \mathbf{a}^{[L]}}{\partial A^{[L-1]}}.
$$

Plugging this in, we get

$$
\frac{\partial \mathcal{C}}{\partial W^{[L-1]}} = 
\frac{d \mathcal{C}}{d \mathbf{a}^{[L]}} \frac{\partial \mathbf{a}^{[L]}}{\partial A^{[L-1]}}
\frac{\partial A^{[L-1]}}{\partial W^{[L-1]}}.
$$

The neat thing about backprop is that we can just repeat this for arbitrarily deep networks. And that is in essence of how deep learning works. It's a slightly ironic thing that one of the most revolutionary technologies of our time is based on high school and first year university mathematics.

## Backpropagation with concrete examples
The equations in the previous section are very general and do not depend on the form of either the cost function or the activation function. Let us do these calculations with some specific cost functions and activation functions.

We will use the log loss function

$$
\mathcal{L}(y,\hat{y}) = y\log \hat{y} + (1-y)\log(1-\hat{y}).
$$

And we define the cost to be

$$
\mathcal{C}(\mathbf{y},\mathbf{\hat{y}}) = \frac{1}{m} \sum_i^m \mathcal{L}(y_i,\hat{y}_i),
$$

where \\(\mathbf{y}\\) and \\(\mathbf{\hat{y}}\\) refer to the actual and predicted labels, respectively (see week 2 course notes for a derivation).

For the final layer, we will use a sigmoid activation function since we're doing binary classification, and all other layers will use RELUs. Differentiating the final layer activation with respect to the cost function, we get

$$
\frac{d \mathcal{C}}{d \mathbf{a}^{[L]}} = \frac{y}{\mathbf{a}^{[L]}} - 
\frac{1-y}{1-\mathbf{a}^{[L]}}
$$

Now, we can work out the derivative of the activation in the final layer with respect to \\(\mathbf{z}^{[L]}\\) (in other words, how does the activation change as we make a very small change in the input to the activation function). Recall that

$$
\mathbf{a}^{[L]} = g^{[L]} ( \mathbf{z}^{[L]}),
$$

where \\(g^{[L]}\\) is the activation function associated with layer \\(L\\). Given that we have assigned \\(g^{[L]}\\) to be a sigmoid,

$$
\frac{d \mathbf{a}^{[L]}}{d \mathbf{z}^{[L]}} = \mathbf{a}^{[L]} \ast (1-\mathbf{a}^{[L]}).
$$

Here, \\(\ast\\) denotes element wise-multiplication of the vector. Now we use the chain rule to work out how the activation changes as we change the weights,

$$
\frac{\partial \mathbf{a}^{[L]}}{\partial \mathbf{w}^{[L]}} =
\frac{d \mathbf{a}^{[L]}}{d \mathbf{z}^{[L]}}
\frac{\partial \mathbf{z}^{[L]}}{\partial \mathbf{w}^{[L]}}.
$$

Of course, since \\(\mathbf{z}^{[L]} = A^{[L-1]} \mathbf{w}^{[L]}\\), we can see that 

$$
\frac{\partial \mathbf{z}^{[L]}}{\partial \mathbf{w}^{[L]}} = A^{[L-1]}.
$$



If we want to compute how the cost changes as we change the weights, we can make use of what we have learned so far:
$$
\begin{align}
\frac{\partial \mathcal{C}}{\partial \mathbf{w^{[L]}}} & =
\frac{d \mathcal{C}}{d \mathbf{a}^{[L]}} \frac{\partial \mathbf{a}^{[L]}}{\partial \mathbf{w}^{[L]}}\\
&=
\frac{d \mathcal{C}}{d \mathbf{a}^{[L]}} 
\frac{d \mathbf{a}^{[L]}}{d \mathbf{z}^{[L]}}
\frac{\partial \mathbf{z}^{[L]}}{\partial \mathbf{w}^{[L]}} \\
&=
\left( \frac{\mathbf{y}}{\mathbf{a}^{[L]}} - \frac{1-\mathbf{y}}{1-\mathbf{a}^{[L]}} \right)
\left( \mathbf{a}^{[L]} \ast (1-\mathbf{a}^{[L]}) \right)  A^{[L-1]}.
\end{align}
$$

There are a lot of moving parts here, so let's just write a quick simulation to make sure we haven't made a mess of anything.


```python
%matplotlib inline
import matplotlib.pyplot as plt
import numpy as np
plt.rc('text',usetex=True)

N,M = 500,1 # number of samples; number of variables
sigmoid = lambda x: 1/(1+np.exp(-x))

y = np.random.choice([0,1],N,replace=True)
z1 = np.random.randn(N,M)

'''
I'm just defining a class for this so that I can have easy
access to the intermediate steps while still having a function
to run the forward pass.
'''
class MiniNet:
    def __init__(self,N,M):
        self.w2 = np.random.randn(M)
        
    def forward_pass(self,z1):
        self.a1 = (z1>0)*z1
        self.z2 = self.w2.dot(self.a1.T)
        self.a2 = sigmoid(self.z2)
        return self.a2

myNet = MiniNet(N,M)
yHat = myNet.forward_pass(z1)

cost = lambda y,yHat:  y * np.log(yHat) + (1-y)*np.log(1-yHat)


''' These are the analytical terms we worked out above '''
dcda2 = (y/yHat - (1-y)/(1-yHat) ) 
da2dz2 = yHat * (1-yHat)
dz2dw2 = myNet.a1[:,0]
dcdw2 = dcda2 * da2dz2 * dz2dw2

dcAnalytical = dcdw2

''' Now let's get the empirical results from simply adjusting
the weights a little bit and computing the cost as we change them '''
dw = 1e-10

weights = myNet.w2

myNet.w2 = weights + dw
cost1 = cost(y,myNet.forward_pass(z1))

myNet.w2 = weights - dw
cost2 = cost(y,myNet.forward_pass(z1))

dcEmpirical = (cost1-cost2)/(2*dw)


fig,ax = plt.subplots(1,figsize=(6.5,5.5))
ax.plot(dcEmpirical,dcEmpirical,'-',lw=1,c='k')
ax.scatter(dcEmpirical,dcAnalytical,c='r',s=80)
ax.set_xlabel(r'$\frac{\partial \mathcal{C}}{\partial \mathbf{w^{[L]}}}$ (simulation)',fontsize=30)
ax.set_ylabel(r'$\frac{\partial \mathcal{C}}{\partial \mathbf{w^{[L]}}}$ (analytical)',fontsize=30)
set_ticklabelsize(axs,24)
```


![png](output_8_0.png)


Wonderful stuff! Our analytical result agrees with our simulations which gives us high confidence that we've done the derivation (and implementation) correctly. If we need to backpropagate our errors further back into the network, we would again simply apply the chain rule. For example, for the weights in layer L-1, we could compute

$$
\frac{\partial \mathcal{C}}{\partial W^{[L-1]}} =
\frac{\partial \mathcal{C}}{\partial A^{[L-1]}}
\frac{\partial A^{[L-1]}}{\partial W^{[L-1]}},
$$

and then apply the negative of these gradients to the weights reduce the cost. Let us quickly work out these terms,

$$
\frac{\partial \mathcal{C}}{\partial A^{[L-1]}} = 
\frac{d \mathcal{C}}{d \mathbf{a}^{[L]}}
\frac{\partial \mathbf{a}^{[L]}}{\partial A^{[L-1]}},
$$

with \\(\frac{\partial \mathcal{C}}{\partial \mathbf{a}^{[L]}}\\) being as before, and

$$
\frac{\partial \mathbf{a}^{[L]}}{\partial A^{[L-1]}} = 
\frac{d \mathbf{a}^{[L]}}{d \mathbf{z}^{[L]}}
\frac{\partial \mathbf{z}^{[L]}}{\partial A^{[L-1]}}
$$

We can also see that

$$
\frac{\partial \mathbf{z}^{[L]}}{\partial A^{[L-1]}} = W^{[L-1]}.
$$

Finally, we need to know how the activation changes in the current layer as the weights change

$$
\frac{\partial A^{[L-1]}}{\partial W^{[L-1]}} = \max(0,Z^{[L-1]})
$$

Now, putting all of this together, 
$$
\frac{\partial \mathcal{C}}{\partial A^{[L-1]}} =
\frac{d \mathcal{C}}{d \mathbf{a}^{[L]}}
\frac{d \mathbf{a}^{[L]}}{d \mathbf{z}^{[L]}}
\frac{\partial \mathbf{z}^{[L]}}{\partial A^{[L-1]}},
$$

which means that

$$
\frac{\partial \mathcal{C}}{\partial W^{[L-1]}}
= \frac{d \mathcal{C}}{d \mathbf{a}^{[L]}}
\frac{d \mathbf{a}^{[L]}}{d \mathbf{z}^{[L]}}
\frac{\partial \mathbf{z}^{[L]}}{\partial A^{[L-1]}}
\frac{\partial A^{[L-1]}}{\partial W^{[L-1]}},
$$

where 

$$\frac{d \mathcal{C}}{d \mathbf{a}^{[L]}} = \frac{y}{\mathbf{a}^{[L]}} - 
\frac{1-y}{1-\mathbf{a}^{[L]}},
$$

$$
\frac{d \mathbf{a}^{[L]}}{d \mathbf{z}^{[L]}} = \mathbf{a}^{[L]} \ast (1-\mathbf{a}^{[L]}),
$$

$$
\frac{\partial \mathbf{z}^{[L]}}{\partial A^{[L-1]}} = W^{[L-1]},
$$

and

$$
\frac{\partial A^{[L-1]}}{\partial W^{[L-1]}} = \max(0,Z^{[L-1]}).
$$

As we can see, this procedure can simply be repeated *ad nauseum*. The nice thing about backprop
is that once you have the cost derivative in a layer, it is straightforward to
propagate it back to the preceding layer. In the next set of notes for this course, I will
implement these ideas in Python beyond the sort of rough and ready "MiniNet" I did above.